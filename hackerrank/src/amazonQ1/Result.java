package amazonQ1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Result {

	public static void main(String[] args) {
		
		//sample inputs 
		List<Integer> parts = new ArrayList<>();
		parts.add(6);
		parts.add(10);
		parts.add(23);
		parts.add(67);
		parts.add(45);
		
		int tot = combineParts(parts);
		System.out.println(tot);
	}
    //question : Minimum time required to assemble all parts
	private static int combineParts(List<Integer> parts) {
		int i = parts.size();
		if(i == 1)
		{
			return 0;
		}
		
		int j=0;
		
		List<Integer> sum = new ArrayList<Integer>();
		while(i>1) {
			parts.sort(Comparator.naturalOrder()); 
			int small = parts.get(0); 
			int small2 = parts.get(1); 
			sum.add(j,(small + small2) );
			if(i!=2) {                                           
			parts.remove(0);                            
			parts.remove(0);
			}                                      
			parts.add(sum.get(j));    
			i--; //3 ,2
			j++; //1 ,2
			System.out.println(sum);
		}
		int total = 0;
		for(int k=0;k<sum.size();k++) {
			total = total + sum.get(k);
		}
		return total;
		
	}

}
