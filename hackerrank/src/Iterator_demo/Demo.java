package Iterator_demo;

import java.util.ArrayList;
import java.util.Iterator;

public class Demo {

	public static void main(String[] args) {
		
		ArrayList myList = new ArrayList();
		
		myList.add("Hello");
		myList.add("Java");
		myList.add("3");
		
		Iterator it = myList.iterator();
		
		while(it.hasNext())
		{
			Object element = it.next();
			System.out.println((String)element);
		}

	}

}
